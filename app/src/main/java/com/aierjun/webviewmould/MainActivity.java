package com.aierjun.webviewmould;

import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Environment;
import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends Activity {
	public String filePath_xlsx = Environment.getExternalStorageDirectory()
			+ "/case.xlsx";
	public String filePath_docx = Environment.getExternalStorageDirectory()
			+ "/22[1].docx";
	public String filePath_doc = Environment.getExternalStorageDirectory()
			+ "/11[1].doc";
	public String filePath_xls = Environment.getExternalStorageDirectory()
			+ "/case.xls";

	private Button btn_load_xlsx;
	private Button btn_load_doc;
	private Button btn_load_docx;
	private Button btn_load_xls;

	private TextView readTextView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		btn_load_xlsx = (Button) findViewById(R.id.btn_load);
		btn_load_xlsx.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent i = new Intent();
				i.setClass(MainActivity.this, FileReadActivity.class);
				Bundle bundle = new Bundle();
				bundle.putString("filePath", filePath_xlsx);
				i.putExtras(bundle);
				startActivity(i);
			}
		});
		
		btn_load_xls = (Button) findViewById(R.id.btn_load_xls);
		btn_load_xls.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent i = new Intent();
				i.setClass(MainActivity.this, FileReadActivity.class);
				Bundle bundle = new Bundle();
				bundle.putString("filePath", filePath_xls);
				i.putExtras(bundle);
				startActivity(i);
			}
		});
	    
		btn_load_doc = (Button) findViewById(R.id.btn_load_doc);
		btn_load_doc.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent i = new Intent();
				i.setClass(MainActivity.this, FileReadActivity.class);
				Bundle bundle = new Bundle();
				bundle.putString("filePath", filePath_doc);
				i.putExtras(bundle);
				startActivity(i);
			}
		});
		
		btn_load_docx = (Button) findViewById(R.id.btn_load_docx);
		btn_load_docx.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent i = new Intent();
				i.setClass(MainActivity.this, FileReadActivity.class);
				Bundle bundle = new Bundle();
				bundle.putString("filePath", filePath_docx);
				i.putExtras(bundle);
				startActivity(i);
			}
		});

		readTextView= (TextView) findViewById(R.id.readtextview);

		readTextView.setText("哈哈镜肯德基阿里123哈哈镜肯德基阿里123哈哈镜肯德基阿里123哈哈镜肯德基阿里123哈哈镜肯德基阿里123哈哈镜肯德基阿里123哈哈镜肯德基阿里123哈哈镜肯德基阿里123");
		Log.d("Jun",getTextWidth("哈哈镜肯德基阿里123")+"..."+getTextHeight("哈哈镜肯德基阿里123")+"..."+getTextWidth2("哈哈镜肯德基阿里123")+"..."+readTextView.getText().length()+"...");
		
	}
	public int getFontHeight(float fontSize)
	{
		Paint paint = new Paint();
		paint.setTextSize(fontSize);
		Paint.FontMetrics fm = paint.getFontMetrics();
		return (int) Math.ceil(fm.descent - fm.ascent);
	}
	public int getTextWidth(String text){
		Paint paint=new Paint();
		Rect rect = new Rect();
		paint.getTextBounds(text,0,text.length(), rect);
		return  rect.width();
	}
	public int getTextHeight(String text){
		Paint paint=new Paint();
		Rect rect = new Rect();
		paint.getTextBounds(text,0,text.length(), rect);
		return rect.height();
	}
	public float getTextWidth2(String text){
		Paint paint=new Paint();
		Rect rect = new Rect();
		return  paint.measureText(text,0,text.length());
	}
}
