package com.aierjun.webviewmould;

import android.content.Context;
import android.content.res.AssetManager;
import android.text.Layout;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.aierjun.webviewmould.text.CharsetDetector;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.CharBuffer;
import java.nio.charset.Charset;

/**
 * Created by Administrator on 2017/8/8 0008.
 */

public class ReadTextView extends TextView {
    BufferedReader reader;
    int maxPosition = 0;
    CharBuffer buffer = null;
    int position = 0;
    PageCallback pageCallback = null;
    int page = 1;

    public ReadTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public ReadTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ReadTextView(Context context) {
        super(context);
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);
        resize();
    }

    /**
     * 取出当前页无法显示的字
     *
     * @return 去掉的字数
     */
    public int resize() {
        CharSequence oldContent = getText();
        CharSequence newContent = oldContent.subSequence(0, getCharNum());
        setText(newContent);
//        page =  (int) (position/getCharNum()+0.99);
        return oldContent.length() - newContent.length();
    }

    /**
     * 获取当前页总字数
     */
    public int getCharNum() {
        return getLayout().getLineEnd(getLineNum());
    }

    /**
     * 获取当前页总行数
     */
    public int getLineNum() {
        Layout layout = getLayout();
        int topOfLastLine = getHeight() - getPaddingTop() - getPaddingBottom() - getLineHeight();
        return layout.getLineForVertical(topOfLastLine);
    }

    public ReadTextView loadInputStream(InputStream in) {
        try {
            Charset charset = CharsetDetector.detect(in);
            buffer = CharBuffer.allocate(maxPosition);
            reader = new BufferedReader(new InputStreamReader(in, charset));
            reader.read(buffer);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return this;
    }

    public ReadTextView loadPagePosition(int pagePosition) {
        this.position = pagePosition;
        buffer.position(position);
        setText(buffer);
        return this;
    }

    /**
     * 上一页按钮
     */
    public void previewPageBtn() {
        if (page > 1)
            page--;
        position -= getCharNum();
        if (position == 0) {
            page = 1;
            Log.d("ReadTextView", "已到达第一页");
        }
        if (position <= 0)
            position = 0;
        loadPagePosition(position);
        resize();
        if (pageCallback != null)
            pageCallback.callBack(page);
    }

    /**
     * 下一页按钮
     */
    public void nextPageBtn() {

        position += getCharNum();
        if (maxPosition == 0) {
            Log.d("ReadTextView", "请在loadBook()之前调用setTextStrLength()");
            return;
        }
        if (position >= maxPosition)
            position = maxPosition;
        loadPagePosition(position);
        resize();
        if (position != maxPosition)
            page++;
        if (pageCallback != null)
            pageCallback.callBack(page);
    }

    public ReadTextView setTextStrLength(int maxPosition) {
        this.maxPosition = maxPosition;
        return this;
    }

    public void setPageCallback(PageCallback pageCallback) {
        this.pageCallback = pageCallback;
    }

    public interface PageCallback {
        void callBack(int page);
    }
}

