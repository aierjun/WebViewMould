package com.aierjun.webviewmould.sql;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by aierJun on 2017/2/7.
 */
public class DBHelper extends SQLiteOpenHelper {

    public final static String DB_NAME="Works.db";  //数据库名称

    public final static String TABLE_NAME="works";  // 表名称

//    public final static String FIELD_ID="_ID";      //主键

    public final static String FIELD_IP="_IP";   //表列名

    public final static String FIELD_PAGE="_PAGE";  //表列名

    public final static String FIELD_INDEX="_INDEX";



    public DBHelper(Context context, SQLiteDatabase.CursorFactory factory, int version) {
        super(context,DB_NAME, factory, version);
        //第一个参数是context上下文

        // 第二个参数是数据库名，

        // 第三个参数是游标工厂，用来产生游标，作用是对查询的结果集进行随机访问，传入null代表使用系统默认的游标对象

        //第四个是版本号。可以用常量定义（如：private static final int DB_VERSION = 2;）

    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String sql="create table "
                +TABLE_NAME+"("
                +FIELD_IP+" integer primary key, "
                +FIELD_PAGE+" varchar, "
                +FIELD_INDEX+" varchar)";
        Log.d("JunLog","onCreate");
        db.execSQL(sql);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {  //数据库文件的版本号发生变更时调用

    }

    public  void insert(String ip, String page,String index){  //插入数据
        SQLiteDatabase db =this.getWritableDatabase();
        ContentValues cv=new ContentValues();
        cv.put(FIELD_IP,ip);
        cv.put(FIELD_PAGE,page);
        cv.put(FIELD_INDEX,index);
        db.insert(TABLE_NAME,null,cv);
        db.close();
        Log.d("JunLog","Insert data");
    }

    public void updata(int ip, String page,String index){  //插入列
        SQLiteDatabase db=this.getWritableDatabase();
        String where=FIELD_IP+"=?";
        String[] whereValue={Integer.toString(ip)};
        ContentValues cv=new ContentValues();    //各个字段的数据使用ContentValues进行存放
        cv.put(FIELD_PAGE,page);
        cv.put(FIELD_INDEX,index);
        db.update(TABLE_NAME,cv,where,whereValue);
        Log.d("JunLog","Modify data");
    }

    public void delete(int ip){                         //删除行数据（指定id的行的数据）
        SQLiteDatabase db=this.getWritableDatabase();
        String where=FIELD_IP+"=?";
        String[] whereValue={Integer.toString(ip)};
        db.delete(TABLE_NAME,where,whereValue);
        Log.d("JunLog","Delete data");
    }

    public boolean deleteDBByName(String DBName){          //删除数据库
        Context context=null;
        context.deleteDatabase(DBName);
        return  false;
    }

    public Cursor getCursor(){                               //获取游标（理解为找到表）
        SQLiteDatabase db=this.getWritableDatabase();
        Cursor cursor=db.query(TABLE_NAME  ,null,null,null,null,null,null);
        return cursor;
    }

    public Cursor quareSQLBYIP(String ip){
        SQLiteDatabase db=this.getWritableDatabase();
        Cursor cursor=db.rawQuery("select * from "+TABLE_NAME+" where _IP="+ip, null);
        return cursor;
    }
}
