package com.aierjun.webviewmould.text;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.CharBuffer;
import java.nio.charset.Charset;

import android.app.Activity;
import android.content.Intent;
import android.content.res.AssetManager;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;

import com.aierjun.webviewmould.R;
import com.aierjun.webviewmould.ReadTextView;
import com.aierjun.webviewmould.sql.DBHelper;
import com.aierjun.webviewmould.utils.CallBackReceiver;
import com.aierjun.webviewmould.utils.PhoneChargingService;
import com.aierjun.webviewmould.utils.PhoneConditionMonitoringReceiver;
import com.aierjun.webviewmould.utils.PhoneUtils;

public class MainActivity extends Activity {

	private ReadTextView readView;
	private Button one;
	private Button two;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main_text);
		
		readView = (ReadTextView) findViewById(R.id.read_view);

		AssetManager assets = getAssets();
		InputStream in = null;
		try {
			in = assets.open("documents/The Golden Compass.txt");
		} catch (IOException e) {
			e.printStackTrace();
		}
		readView.setTextStrLength(8000)
				.loadInputStream(in)
				.loadPagePosition(0);

		readView.setPageCallback(new ReadTextView.PageCallback() {
			@Override
			public void callBack(int page) {
				Log.d("Jun",page+"....page");
			}
		});

		one = (Button) findViewById(R.id.one);
		two = (Button) findViewById(R.id.two);
		one.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				readView.previewPageBtn();
			}
		});
		two.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				readView.nextPageBtn();
				Log.d("Jun",PhoneUtils.getPhoneCharging(MainActivity.this) +"..是否充电..");
				Log.d("Jun",PhoneUtils.getPhoneChargingPowerPercentage(MainActivity.this) +"..充电百分比..");
				Log.d("Jun",PhoneUtils.checkNetworkType(MainActivity.this) +"..网络类型..");
				Log.d("Jun",PhoneUtils.getOperatorName(MainActivity.this)+"..网络String..");

				startService(new Intent(MainActivity.this,PhoneChargingService.class));

			}
		});
		PhoneConditionMonitoringReceiver.callBackReceiver=new CallBackReceiver() {
			@Override
			public void callBackNetType(int type) {
				Log.d("Jun",type +"..网络类型.1.");
			}

			@Override
			public void callBackNetOperator(String s) {
				Log.d("Jun",s+"..网络String.1.");
			}

			@Override
			public void callBackCharging(boolean charging) {
				Log.d("Jun",charging +"..是否充电.1.");
			}

			@Override
			public void callBackChargingPowerPercentage(float power) {
				Log.d("Jun",power +"..充电百分比.1.");
			}
		};
	}

	private String sql(){
		DBHelper dbHelper=new DBHelper(this,null,1);
		dbHelper.insert("1234","5","3000");
		Cursor cursor = dbHelper.quareSQLBYIP("1234");
		while (cursor.moveToNext()){
			String name = cursor.getString(cursor.getColumnIndex("_INDEX"));
			return name+"";
		}
		return null;
	}
}
