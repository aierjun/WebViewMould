package com.aierjun.webviewmould.utils;

/**
 * Created by Administrator on 2017/8/9 0009.
 */

public interface CallBackReceiver {
    public void callBackNetType(int type);

    public void callBackNetOperator(String s);

    public void callBackCharging(boolean charging);

    public void callBackChargingPowerPercentage(float power);
}
