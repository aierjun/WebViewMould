package com.aierjun.webviewmould.utils;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import com.aierjun.webviewmould.text.MainActivity;

/**
 * Created by Administrator on 2017/8/9 0009.
 */

public class PhoneChargingService extends Service {
    @Override
    public IBinder onBind(Intent intent) {
        Log.d("Jun","onBind()");

        return null;
    }

    @Override
    public int onStartCommand(final Intent intent, int flags, int startId) {
        Log.d("Jun","onStartCommand()");
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    while (true){
                        Thread.sleep(10000);
                        //Log.d("Jun",PhoneUtils.getPhoneCharging(getApplication()) +"..是否充电..");
                        //Log.d("Jun",PhoneUtils.getPhoneChargingPowerPercentage(getApplication()) +"..充电百分比..");
                        Intent intent1=new Intent();
                        intent1.setAction("aierjun.action.BATTERY_CHANGED_AIERJUN");
                        intent1.putExtra("Charging",PhoneUtils.getPhoneCharging(getApplication()));
                        intent1.putExtra("ChargingPowerPercentage",PhoneUtils.getPhoneChargingPowerPercentage(getApplication()));
                        intent1.setClass(PhoneChargingService.this,PhoneConditionMonitoringReceiver.class);
                        sendBroadcast(intent1);
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();
        return START_REDELIVER_INTENT;
    }
}
