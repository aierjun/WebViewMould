package com.aierjun.webviewmould.utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.util.Log;
import android.widget.Toast;

import com.aierjun.webviewmould.text.MainActivity;

/**
 * Created by Administrator on 2017/8/9 0009.
 */

public class PhoneConditionMonitoringReceiver extends BroadcastReceiver {
    public static CallBackReceiver callBackReceiver=null;
    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals(ConnectivityManager.CONNECTIVITY_ACTION)) {
//            Log.d("Jun",PhoneUtils.checkNetworkType(context) +"..网络类型..");
//            Log.d("Jun",PhoneUtils.getOperatorName(context)+"..网络String..");
            if (callBackReceiver !=null ){
                callBackReceiver.callBackNetType(PhoneUtils.checkNetworkType(context));
                callBackReceiver.callBackNetOperator(PhoneUtils.getOperatorName(context));
            }
            // 接口回调传过去状态的类型
        }
        if (intent.getAction().equals("aierjun.action.BATTERY_CHANGED_AIERJUN")){
//            Log.d("Jun",intent.getBooleanExtra("Charging",false)+"..是否充电..");
//            Log.d("Jun",intent.getFloatExtra("ChargingPowerPercentage",0)+"..充电百分比..");
            if (callBackReceiver !=null ){
                callBackReceiver.callBackCharging(intent.getBooleanExtra("Charging",false));
                callBackReceiver.callBackChargingPowerPercentage(intent.getFloatExtra("ChargingPowerPercentage",0));
            }
        }
    }
}
