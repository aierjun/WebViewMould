package com.aierjun.webviewmould.utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.BatteryManager;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;

import static android.content.ContentValues.TAG;
import static android.content.Context.TELEPHONY_SERVICE;
import static android.content.Context.WIFI_SERVICE;

/**
 * Created by Administrator on 2017/8/9 0009.
 */

public class PhoneUtils {

    /*
    *  是否在充电
    *
    *  */
    public static boolean getPhoneCharging(Context context){
        IntentFilter ifilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        Intent batteryStatus = context.registerReceiver(null, ifilter);
//你可以读到充电状态,如果在充电，可以读到是usb还是交流电

        // 是否在充电
        int status = batteryStatus.getIntExtra(BatteryManager.EXTRA_STATUS, -1);
        boolean isCharging = status == BatteryManager.BATTERY_STATUS_CHARGING ||
                status == BatteryManager.BATTERY_STATUS_FULL;

        return isCharging;
    }

    /*
    * 怎么充
    * 1.usb
    * 2.交流电
    * */
    public static int getPhoneChargingType(Context context){
        IntentFilter ifilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        Intent batteryStatus = context.registerReceiver(null, ifilter);
//你可以读到充电状态,如果在充电，可以读到是usb还是交流电

        // 是否在充电
        int status = batteryStatus.getIntExtra(BatteryManager.EXTRA_STATUS, -1);
        boolean isCharging = status == BatteryManager.BATTERY_STATUS_CHARGING ||
                status == BatteryManager.BATTERY_STATUS_FULL;

        // 怎么充
        int chargePlug = batteryStatus.getIntExtra(BatteryManager.EXTRA_PLUGGED, -1);
        boolean usbCharge = chargePlug == BatteryManager.BATTERY_PLUGGED_USB;
        boolean acCharge = chargePlug == BatteryManager.BATTERY_PLUGGED_AC;
        if (chargePlug ==  BatteryManager.BATTERY_PLUGGED_USB)
            chargePlug = 1;
        if (chargePlug ==  BatteryManager.BATTERY_PLUGGED_AC)
            chargePlug =2;
        return chargePlug;
    }

    /*
    *  电量百分比
    *
    *  */
    public static float getPhoneChargingPowerPercentage(Context context){
        IntentFilter ifilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        Intent batteryStatus = context.registerReceiver(null, ifilter);
        //当前剩余电量
        int level = batteryStatus.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
        //电量最大值
        int scale = batteryStatus.getIntExtra(BatteryManager.EXTRA_SCALE, -1);
        //电量百分比
        float batteryPct = level / (float)scale;

        return batteryPct;
    }



    /***
     * 判断Network具体类型（联通移动wap，电信wap，其他net）
     *
     * */
    private static final String CTWAP = "ctwap";
    private static final String CTNET = "ctnet";
    private static final String CMWAP = "cmwap";
    private static final String CMNET = "cmnet";
    private static final String NET_3G = "3gnet";
    private static final String WAP_3G = "3gwap";
    private static final String UNIWAP = "uniwap";
    private static final String UNINET = "uninet";

    private static final int TYPE_CT_WAP = 5;
    private static final int TYPE_CT_NET = 6;
    private static final int TYPE_CT_WAP_2G = 7;
    private static final int TYPE_CT_NET_2G = 8;

    private static final int TYPE_CM_WAP = 9;
    private static final int TYPE_CM_NET = 10;
    private static final int TYPE_CM_WAP_2G = 11;
    private static final int TYPE_CM_NET_2G = 12;

    private static final int TYPE_CU_WAP = 13;
    private static final int TYPE_CU_NET = 14;
    private static final int TYPE_CU_WAP_2G = 15;
    private static final int TYPE_CU_NET_2G = 16;

    private static final int TYPE_OTHER = 17;

    private static Uri PREFERRED_APN_URI = Uri
            .parse("content://telephony/carriers/preferapn");

    /** 没有网络 */
    private static final int TYPE_NET_WORK_DISABLED = 0;

    /** wifi网络 */
    private static final int TYPE_WIFI = 4;

    public static int checkNetworkType(Context mContext) {
        try {
            final ConnectivityManager connectivityManager = (ConnectivityManager) mContext
                    .getSystemService(Context.CONNECTIVITY_SERVICE);
            final NetworkInfo mobNetInfoActivity = connectivityManager
                    .getActiveNetworkInfo();
            if (mobNetInfoActivity == null || !mobNetInfoActivity.isAvailable()) {
                // 注意一：
                // NetworkInfo 为空或者不可以用的时候正常情况应该是当前没有可用网络，
                // 但是有些电信机器，仍可以正常联网，
                // 所以当成net网络处理依然尝试连接网络。
                // （然后在socket中捕捉异常，进行二次判断与用户提示）。
                return TYPE_NET_WORK_DISABLED;
            } else {
                // NetworkInfo不为null开始判断是网络类型
                int netType = mobNetInfoActivity.getType();
                if (netType == ConnectivityManager.TYPE_WIFI) {
                    // wifi net处理
                    return TYPE_WIFI;
                } else if (netType == ConnectivityManager.TYPE_MOBILE) {
                    // 注意二：
                    // 判断是否电信wap:
                    // 不要通过getExtraInfo获取接入点名称来判断类型，
                    // 因为通过目前电信多种机型测试发现接入点名称大都为#777或者null，
                    // 电信机器wap接入点中要比移动联通wap接入点多设置一个用户名和密码,
                    // 所以可以通过这个进行判断！

                    boolean is3G = isFastMobileNetwork(mContext);

                    final Cursor c = mContext.getContentResolver().query(
                            PREFERRED_APN_URI, null, null, null, null);
                    if (c != null) {
                        c.moveToFirst();
                        final String user = c.getString(c
                                .getColumnIndex("user"));
                        if (!TextUtils.isEmpty(user)) {
                            if (user.startsWith(CTWAP)) {
                                return is3G ? TYPE_CT_WAP : TYPE_CT_WAP_2G;
                            } else if (user.startsWith(CTNET)) {
                                return is3G ? TYPE_CT_NET : TYPE_CT_NET_2G;
                            }
                        }
                    }
                    c.close();

                    // 注意三：
                    // 判断是移动联通wap:
                    // 其实还有一种方法通过getString(c.getColumnIndex("proxy")获取代理ip
                    // 来判断接入点，10.0.0.172就是移动联通wap，10.0.0.200就是电信wap，但在
                    // 实际开发中并不是所有机器都能获取到接入点代理信息，例如魅族M9 （2.2）等...
                    // 所以采用getExtraInfo获取接入点名字进行判断

                    String netMode = mobNetInfoActivity.getExtraInfo();
                    Log.i("", "==================netmode:" + netMode);
                    if (netMode != null) {
                        // 通过apn名称判断是否是联通和移动wap
                        netMode = netMode.toLowerCase();

                        if (netMode.equals(CMWAP)) {
                            return is3G ? TYPE_CM_WAP : TYPE_CM_WAP_2G;
                        } else if (netMode.equals(CMNET)) {
                            return is3G ? TYPE_CM_NET : TYPE_CM_NET_2G;
                        } else if (netMode.equals(NET_3G)
                                || netMode.equals(UNINET)) {
                            return is3G ? TYPE_CU_NET : TYPE_CU_NET_2G;
                        } else if (netMode.equals(WAP_3G)
                                || netMode.equals(UNIWAP)) {
                            return is3G ? TYPE_CU_WAP : TYPE_CU_WAP_2G;
                        }
                    }
                }
            }

        } catch (Exception ex) {
            ex.printStackTrace();
            return TYPE_OTHER;
        }

        return TYPE_OTHER;

    }
    private static boolean isFastMobileNetwork(Context context) {
        TelephonyManager telephonyManager = (TelephonyManager) context
                .getSystemService(TELEPHONY_SERVICE);

        switch (telephonyManager.getNetworkType()) {
            case TelephonyManager.NETWORK_TYPE_1xRTT:
                return false; // ~ 50-100 kbps
            case TelephonyManager.NETWORK_TYPE_CDMA:
                return false; // ~ 14-64 kbps
            case TelephonyManager.NETWORK_TYPE_EDGE:
                return false; // ~ 50-100 kbps
            case TelephonyManager.NETWORK_TYPE_EVDO_0:
                return true; // ~ 400-1000 kbps
            case TelephonyManager.NETWORK_TYPE_EVDO_A:
                return true; // ~ 600-1400 kbps
            case TelephonyManager.NETWORK_TYPE_GPRS:
                return false; // ~ 100 kbps
            case TelephonyManager.NETWORK_TYPE_HSDPA:
                return true; // ~ 2-14 Mbps
            case TelephonyManager.NETWORK_TYPE_HSPA:
                return true; // ~ 700-1700 kbps
            case TelephonyManager.NETWORK_TYPE_HSUPA:
                return true; // ~ 1-23 Mbps
            case TelephonyManager.NETWORK_TYPE_UMTS:
                return true; // ~ 400-7000 kbps
            case TelephonyManager.NETWORK_TYPE_EHRPD:
                return true; // ~ 1-2 Mbps
            case TelephonyManager.NETWORK_TYPE_EVDO_B:
                return true; // ~ 5 Mbps
            case TelephonyManager.NETWORK_TYPE_HSPAP:
                return true; // ~ 10-20 Mbps
            case TelephonyManager.NETWORK_TYPE_IDEN:
                return false; // ~25 kbps
            case TelephonyManager.NETWORK_TYPE_LTE:
                return true; // ~ 10+ Mbps
            case TelephonyManager.NETWORK_TYPE_UNKNOWN:
                return false;
            default:
                return false;

        }
    }
    private void getPhoneNetWorkSwitch(int checkNetworkType){
        switch (checkNetworkType) {
            case TYPE_WIFI:
                Log.i("NetType", "================wifi");
                break;
            case TYPE_NET_WORK_DISABLED:
                Log.i("NetType", "================no network");
                break;
            case TYPE_CT_WAP:
                Log.i("NetType", "================ctwap");
                break;
            case TYPE_CT_WAP_2G:
                Log.i("NetType", "================ctwap_2g");
                break;
            case TYPE_CT_NET:
                Log.i("NetType", "================ctnet");
                break;
            case TYPE_CT_NET_2G:
                Log.i("NetType", "================ctnet_2g");
                break;
            case TYPE_CM_WAP:
                Log.i("NetType", "================cmwap");
                break;
            case TYPE_CM_WAP_2G:
                Log.i("NetType", "================cmwap_2g");
                break;
            case TYPE_CM_NET:
                Log.i("NetType", "================cmnet");
                break;
            case TYPE_CM_NET_2G:
                Log.i("NetType", "================cmnet_2g");
                break;
            case TYPE_CU_NET:
                Log.i("NetType", "================cunet");
                break;
            case TYPE_CU_NET_2G:
                Log.i("NetType", "================cunet_2g");
                break;
            case TYPE_CU_WAP:
                Log.i("NetType", "================cuwap");
                break;
            case TYPE_CU_WAP_2G:
                Log.i("NetType", "================cuwap_2g");
                break;
            case TYPE_OTHER:
                Log.i("NetType", "================other");
                break;
            default:
                break;
        }
    }
    public void getPhoneNetWorkPrint(Context context){
        getPhoneNetWorkSwitch(checkNetworkType(context));
    }


    /*
    * 运营商
    * */
    public static  String getOperatorName(Context context) {
        TelephonyManager telephonyManager = (TelephonyManager)context.getSystemService(TELEPHONY_SERVICE);
        String operator = telephonyManager.getSimOperator();
        if (operator != null) {
            if (operator.equals("46000") || operator.equals("46002")) {
                // operatorName="中国移动";
               return ("中国移动");
                // Toast.makeText(this, "此卡属于(中国移动)",
                // Toast.LENGTH_SHORT).show();
            } else if (operator.equals("46001")) {
                // operatorName="中国联通";
                return ("中国联通");
                // Toast.makeText(this, "此卡属于(中国联通)",
                // Toast.LENGTH_SHORT).show();
            } else if (operator.equals("46003")) {
                // operatorName="中国电信";
                return ("中国电信");
                // Toast.makeText(this, "此卡属于(中国电信)",
                // Toast.LENGTH_SHORT).show();
            }
        }
        return null;
    }


}
